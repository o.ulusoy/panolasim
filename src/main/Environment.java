package main;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

import main.Agent.ClarkeTaxAgent;

public class Environment {
	
	private int envType;
	//Environment type encoded with an integer
	//0 - Clarke/Tax Environment
	
	
	//change this replace to proper place
	int margWon = 0;
	int margTot = 0;
	int amaWon = 0;
	int amaTot = 0;
	int tecWon = 0;
	int tecTot = 0;
	int lazyWon = 0;
	int lazyTot = 0;
	int funWon = 0;
	int funTot = 0;
	
	int margWonv = 0;
	int amaWonv = 0;
	int tecWonv = 0;
	int lazyWonv = 0;
	int funWonv = 0;
	
	//Single test 
	int tott = 0;
	int Won1 = 0;
	int Tot1 = 0;
	int Won1v = 0;
	int satisf = 0;
	
	enum envTypeText {
		  CLARKETAX
		}
	
	public Environment(int envType) throws IOException{
		this.envType = envType;
		

		PrintStream out = new PrintStream(
		        new FileOutputStream("D:\\outputTest.txt",true));
		System.setOut(out);
	}
	
	public int getEnvType(){
		return this.envType;
	}
	
	public String envTypeText(int index){
		return envTypeText.values()[index].toString();
	}
	
	public void runClarkTax(ArrayList<ClarkeTaxAgent> agents, ArrayList<Content> contents) throws IOException {

		
		FileWriter fw = new FileWriter("D:\\trash\\SuccessMarg.txt", true);
	    BufferedWriter bw = new BufferedWriter(fw);
	    PrintWriter out = new PrintWriter(bw);
	    FileWriter fw2 = new FileWriter("D:\\trash\\SuccessAma.txt", true);
	    BufferedWriter bw2 = new BufferedWriter(fw2);
	    PrintWriter out2 = new PrintWriter(bw2);
	    FileWriter fw3 = new FileWriter("D:\\trash\\SuccessTec.txt", true);
	    BufferedWriter bw3 = new BufferedWriter(fw3);
	    PrintWriter out3 = new PrintWriter(bw3);
	    FileWriter fw4 = new FileWriter("D:\\trash\\SuccessLazy.txt", true);
	    BufferedWriter bw4 = new BufferedWriter(fw4);
	    PrintWriter out4 = new PrintWriter(bw4);
	    FileWriter fw5 = new FileWriter("D:\\trash\\SuccessFun.txt", true);
	    BufferedWriter bw5 = new BufferedWriter(fw5);
	    PrintWriter out5 = new PrintWriter(bw5);
	    
	    FileWriter fwv = new FileWriter("D:\\trash\\vSuccessMarg.txt", true);
	    BufferedWriter bwv = new BufferedWriter(fwv);
	    PrintWriter outv = new PrintWriter(bwv);
	    FileWriter fw2v = new FileWriter("D:\\trash\\vSuccessAma.txt", true);
	    BufferedWriter bw2v = new BufferedWriter(fw2v);
	    PrintWriter out2v = new PrintWriter(bw2v);
	    FileWriter fw3v = new FileWriter("D:\\trash\\vSuccessTec.txt", true);
	    BufferedWriter bw3v = new BufferedWriter(fw3v);
	    PrintWriter out3v = new PrintWriter(bw3v);
	    FileWriter fw4v = new FileWriter("D:\\trash\\vSuccessLazy.txt", true);
	    BufferedWriter bw4v = new BufferedWriter(fw4v);
	    PrintWriter out4v = new PrintWriter(bw4v);
	    FileWriter fw5v = new FileWriter("D:\\trash\\vSuccessFun.txt", true);
	    BufferedWriter bw5v = new BufferedWriter(fw5v);
	    PrintWriter out5v = new PrintWriter(bw5v);
	    
	  //single test
	    FileWriter fw11 = new FileWriter("D:\\1Success.txt", true);
	    BufferedWriter bw11 = new BufferedWriter(fw11);
	    PrintWriter outt1 = new PrintWriter(bw11);
	    
	    FileWriter fws11 = new FileWriter("D:\\1Satisfaction.txt", true);
	    BufferedWriter bws11 = new BufferedWriter(fws11);
	    PrintWriter outts1 = new PrintWriter(bws11);
	    
	    FileWriter fw11v = new FileWriter("D:\\trash\\v1Success.txt", true);
	    BufferedWriter bw11v = new BufferedWriter(fw11v);
	    PrintWriter outt1v = new PrintWriter(bw11v);
	    
		for (int i = 0; i< agents.size(); i++){
			System.out.println("Agent " + i + ": " + agents.get(i).getAgentType());
		}
		for (int i = 0; i< contents.size(); i++){
			System.out.println();
			System.out.println("Content " + i + " coowners: " + contents.get(i).getCoownersString());
			ArrayList<Integer> bids = new ArrayList<Integer>();
			ArrayList<Integer> votes = new ArrayList<Integer>();
			for(int j=0;j<contents.get(i).cowners.size();j++){
				agents.get(contents.get(i).cowners.get(j)).updateorAddBidScore(contents.get(i).getCoownersString());
				int decision = agents.get(contents.get(i).cowners.get(j)).getDecision();
				System.out.println("Agent " + contents.get(i).cowners.get(j) + " actual decision:" + agents.get(contents.get(i).cowners.get(j)).getActualDecision());
				System.out.println("Agent " + contents.get(i).cowners.get(j) + " given decision:" + decision);
				//System.out.println("Agent " + contents.get(i).cowners.get(j) + " bid:" + agents.get(contents.get(i).cowners.get(j)).rangeBid());
				
				if(decision == 0)
					votes.add(-1);
				if(decision == 1)
					votes.add(1);
				if(decision == -1)
					votes.add(0);
				
				
				//Random agent
				//Random r = new Random();
				//int ran = r.nextInt(40)-20;
				//if(contents.get(i).cowners.get(j) < 1)
				//	bids.add(ran);
				
				//Added for test, uncomment below and comment the rest 
				//bids.add(agents.get(contents.get(i).cowners.get(j)).rangeBid(contents.get(i).getCoownersString()));
				if(contents.get(i).cowners.get(j) <1)
					bids.add(agents.get(contents.get(i).cowners.get(j)).rangeBid(contents.get(i).getCoownersString()));
				else{
					if(agents.get(contents.get(i).cowners.get(j)).getPairwiseBidScore(contents.get(i).getCoownersString()) < 0){
						bids.add(0);
					}
					else{
						System.out.println("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
				if(decision == 0)
					bids.add(-Math.min(10, agents.get(contents.get(i).cowners.get(j)).getPairwiseBidScore(contents.get(i).getCoownersString())));
				if(decision == 1)
					bids.add(Math.min(12, agents.get(contents.get(i).cowners.get(j)).getPairwiseBidScore(contents.get(i).getCoownersString())));
				if(decision == -1)
					bids.add(0);}}
				
			}
			int voteTotal = 0;
			for(int j=0;j<votes.size();j++){
				voteTotal+=votes.get(j);
			}
			//System.out.println(votes.size() + "--" + contents.get(i).cowners.size());
			for(int j=0;j<votes.size();j++){
				if(voteTotal>0 && agents.get(contents.get(i).cowners.get(j)).getActualDecision() == 1){
					if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("MARGINALLYCONCERNED") )
						margWonv++;
					if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("AMATEUR") )
						amaWonv++;
					if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("TECHNICIAN") )
						tecWonv++;
					if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("LAZYEXPERT") )
						lazyWonv++;
					if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("FUNDAMENTALIST") )
						funWonv++;
					//single test
					if(tott >= 1000 && contents.get(i).cowners.get(j) == 0)
						Won1v++;
					}
					if(voteTotal<0 && agents.get(contents.get(i).cowners.get(j)).getActualDecision() == 0){
						if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("MARGINALLYCONCERNED") )
							margWonv++;
						if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("AMATEUR") )
							amaWonv++;
						if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("TECHNICIAN") )
							tecWonv++;
						if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("LAZYEXPERT") )
							lazyWonv++;
						if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("FUNDAMENTALIST") )
							funWonv++;
						//single test
						if(tott >= 1000 && contents.get(i).cowners.get(j) == 0)
							Won1v++;
					}
			}
			
			for(int j=0;j<contents.get(i).cowners.size();j++){
				//single test
				if(tott >= 1000 && contents.get(i).cowners.get(j) == 0)
					outt1v.println((double)Won1v/(double)Tot1);
				
				if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("MARGINALLYCONCERNED") )
					outv.println((double)margWonv/(double)margTot);
				if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("AMATEUR") )
					out2v.println((double)amaWonv/(double)amaTot);
				if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("TECHNICIAN") )
					out3v.println((double)tecWonv/(double)tecTot);
				if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("LAZYEXPERT") )
					out4v.println((double)lazyWonv/(double)lazyTot);
				if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("FUNDAMENTALIST") )
					out5v.println((double)funWonv/(double)funTot);
			}
			tott = i;
			auction(agents,contents.get(i).cowners,bids,contents.get(i).getCoownersString());
			for(int j=0;j<contents.get(i).cowners.size();j++){
				//single test
				if(tott >= 1000 && contents.get(i).cowners.get(j) == 0){
					outt1.println((double)Won1/(double)Tot1);
					outts1.println((double)satisf/(double)(Tot1*5));
				}
				
				if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("MARGINALLYCONCERNED") )
					out.println((double)margWon/(double)margTot);
				if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("AMATEUR") )
					out2.println((double)amaWon/(double)amaTot);
				if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("TECHNICIAN") )
					out3.println((double)tecWon/(double)tecTot);
				if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("LAZYEXPERT") )
					out4.println((double)lazyWon/(double)lazyTot);
				if(agents.get(contents.get(i).cowners.get(j)).getAgentType().equals("FUNDAMENTALIST") )
					out5.println((double)funWon/(double)funTot);
			}
		}
		out.close();
		out2.close();
		out3.close();
		out4.close();
		out5.close();
		outv.close();
		out2v.close();
		out3v.close();
		out4v.close();
		out5v.close();
		outt1.close();
		outts1.close();
		outt1v.close();
		System.out.println("---------------------------");
		System.out.println("Final Summary:");
		for (int i = 0; i< agents.size(); i++){
			System.out.println("Agent " + i + ": " + agents.get(i).getAgentType());
			System.out.println("Total auctions entered: " + agents.get(i).gettotalAsked());
			System.out.println("Total responses: " + agents.get(i).gettotalAnswered());
			System.out.println("Total correct responses: " + agents.get(i).gettotalCorrectAnswered());
			System.out.println("Total bids made: " + agents.get(i).gettotalBid());
			System.out.println("Total correct bids: " + agents.get(i).gettotalCorrectBid());
		}
	}
	
	public void auction(ArrayList<ClarkeTaxAgent> agents,ArrayList<Integer> coowners, ArrayList<Integer> bids, String coownersString){
		System.out.println("---Auction started---");
		//added for Learning test comment later
		if(tott >= 1000 ){
			Tot1++;
			System.out.println("Total increased! " + Tot1); 
		}
		
		for(int j=0;j<bids.size();j++){
			
			System.out.println("Agent " + coowners.get(j) + " bid:" +bids.get(j));
			//Do this proper
			if(agents.get(coowners.get(j)).getAgentType().equals("MARGINALLYCONCERNED") )
				margTot++;
			if(agents.get(coowners.get(j)).getAgentType().equals("AMATEUR") )
				amaTot++;
			if(agents.get(coowners.get(j)).getAgentType().equals("TECHNICIAN") )
				tecTot++;
			if(agents.get(coowners.get(j)).getAgentType().equals("LAZYEXPERT") )
				lazyTot++;
			if(agents.get(coowners.get(j)).getAgentType().equals("FUNDAMENTALIST") )
				funTot++;
			
			//single test commented for Learning test uncomment later
			//if(tott >= 1000 && coowners.get(j) == 0){
			//	Tot1++;
			//	System.out.println("Total increased! " + Tot1); 
			//}
		}
		int bidResult = 0;
		int tempBidResult = 0;
		
		for(int j=0;j<bids.size();j++){
			bidResult += bids.get(j);
		}
		if(bidResult == 0){
			System.out.println("Content indecisive");
			if(tott >= 1000)
			satisf+=3;
		}
		if(bidResult > 0){
			System.out.println("Decision is share");
			
			//if(tott >= 1000)
			//	Tot1++;
			
			if(tott >= 1000)
				satisf+=4;
			
			for(int j=0;j<bids.size();j++){
				int payment = 0;
				int tax = 0;
				tempBidResult = bidResult;
				if(bids.get(j)>0){
					System.out.println("Agent" + coowners.get(j) + " will pay:" + bids.get(j));
					payment = bids.get(j);
					tempBidResult -= bids.get(j);
					if(tempBidResult<=0){
						System.out.println("Agent" + coowners.get(j) + " will be taxed:" + -tempBidResult);
						tax = -tempBidResult;
					}
					
					//single test
					if(tott >= 1000 && coowners.get(j) == 0 && agents.get(coowners.get(j)).getActualDecision() == 1){
						Won1++;
						System.out.println("WON!" + Won1);
					}
					
					if(agents.get(coowners.get(j)).getAgentType().equals("MARGINALLYCONCERNED") ){
						if(agents.get(coowners.get(j)).getActualDecision() == 1)
						margWon++;
						double coef = 1 - 0.5*(double)payment/20 - 0.5*(double)tax/20;
						agents.get(coowners.get(j)).updateEfficienyShRange(coef);
					}
					if(agents.get(coowners.get(j)).getAgentType().equals("AMATEUR") ){
						if(agents.get(coowners.get(j)).getActualDecision() == 1)
						amaWon++;
						double coef = 1 - 0.5*(double)payment/20 - 0.5*(double)tax/20;
						agents.get(coowners.get(j)).updateEfficienyShRange(coef);
					}
					if(agents.get(coowners.get(j)).getAgentType().equals("TECHNICIAN") ){
						if(agents.get(coowners.get(j)).getActualDecision() == 1)
						tecWon++;
						double coef = 1 - 0.5*(double)payment/20 - 0.5*(double)tax/20;
						agents.get(coowners.get(j)).updateEfficienyShRange(coef);
					}
					if(agents.get(coowners.get(j)).getAgentType().equals("LAZYEXPERT") ){
						if(agents.get(coowners.get(j)).getActualDecision() == 1)
						lazyWon++;
						double coef = 1 - 0.5*(double)payment/20 - 0.5*(double)tax/20;
						agents.get(coowners.get(j)).updateEfficienyShRange(coef);
					}
					if(agents.get(coowners.get(j)).getAgentType().equals("FUNDAMENTALIST") ){
						if(agents.get(coowners.get(j)).getActualDecision() == 1)
						funWon++;
						double coef = 1 - 0.0*(double)payment/20 - 0.5*(double)tax/20;
						agents.get(coowners.get(j)).updateEfficienyShRange(coef);
					}
					//double coef = 1 - 0.5*(double)payment/20 - 0.5*(double)tax/20;
					//agents.get(coowners.get(j)).updateEfficienyShRange(coef);
				}
				agents.get(coowners.get(j)).modifyBidScore(coownersString, payment + tax);
			}
		}
		if(bidResult < 0){
			System.out.println("Decision is no share");
			
			//if(tott >= 1000)
				//Tot1++;
			
			if(tott >= 1000)
				satisf+=2;
			
			for(int j=0;j<bids.size();j++){
				int payment = 0;
				int tax = 0;
				tempBidResult = bidResult;
				if(bids.get(j)<0){
					System.out.println("Agent" + coowners.get(j) + " will pay:" + -bids.get(j));
					payment = -bids.get(j);
					tempBidResult -= bids.get(j);
					if(tempBidResult>=0){
						System.out.println("Agent" + coowners.get(j) + " will be taxed:" + tempBidResult);
						tax = tempBidResult;
					}
					
					//single test
					if(tott >= 1000 && coowners.get(j) == 0 && agents.get(coowners.get(j)).getActualDecision() == 0){
						Won1++;
						System.out.println("WON!" + Won1);
					}
					
					
					if(agents.get(coowners.get(j)).getAgentType().equals("MARGINALLYCONCERNED") ){
						if(agents.get(coowners.get(j)).getActualDecision() == 0)
						margWon++;
						double coef = 1 - 0.5*(double)payment/20 - 0.5*(double)tax/20;
						agents.get(coowners.get(j)).updateEfficienyNoShRange(coef);
					}
					if(agents.get(coowners.get(j)).getAgentType().equals("AMATEUR") ){
						if(agents.get(coowners.get(j)).getActualDecision() == 0)
						amaWon++;
						double coef = 1 - 0.5*(double)payment/20 - 0.5*(double)tax/20;
						agents.get(coowners.get(j)).updateEfficienyNoShRange(coef);
					}
					if(agents.get(coowners.get(j)).getAgentType().equals("TECHNICIAN") ){
						if(agents.get(coowners.get(j)).getActualDecision() == 0)
						tecWon++;
						double coef = 1 - 0.5*(double)payment/20 - 0.5*(double)tax/20;
						agents.get(coowners.get(j)).updateEfficienyNoShRange(coef);
					}
					if(agents.get(coowners.get(j)).getAgentType().equals("LAZYEXPERT") ){
						if(agents.get(coowners.get(j)).getActualDecision() == 0)
						lazyWon++;
						double coef = 1 - 0.5*(double)payment/20 - 0.5*(double)tax/20;
						agents.get(coowners.get(j)).updateEfficienyNoShRange(coef);
					}
					if(agents.get(coowners.get(j)).getAgentType().equals("FUNDAMENTALIST") ){
						if(agents.get(coowners.get(j)).getActualDecision() == 0)
						funWon++;
						double coef = 1 - 0.0*(double)payment/20 - 0.5*(double)tax/20;
						agents.get(coowners.get(j)).updateEfficienyNoShRange(coef);
					}
					//double coef = 1 - 0.5*(double)payment/20 - 0.5*(double)tax/20;
					//agents.get(coowners.get(j)).updateEfficienyNoShRange(coef);
				}
				agents.get(coowners.get(j)).modifyBidScore(coownersString, payment + tax);
			}
		}
		
		System.out.println(satisf + "!!!!!!!!!!!! " + Tot1); 
	}

}
