package main;

import main.Environment;
import main.Agent;
import main.Agent.ClarkeTaxAgent;
import main.Content;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Collections;

public class SimRunner {

	public static void main(String[] args) throws IOException {
		// Runs the simulation
		// Initialize and add agents with personas
		// Initialize and add content
		// Assign agent co-owners to content
		// Run auctions for content
		// Agents learn privacy decisions for better bidding
		// Evaluate success of agents
		

		// Confidence interval start
		ArrayList<Double> lastRun = new ArrayList<Double>();
		double lastRunTracker=0;
			BufferedReader reader;
			try {
				reader = new BufferedReader(new FileReader(
						"D:\\L\\MC1Success.txt"));
				String line = "-";
				int counter=1;
				while (line != null) {
					//System.out.println(ct +"-" + line);
					// read next line
					line = reader.readLine();
					if((counter % 1000) == 0){
						lastRunTracker=Double.valueOf(line);
						lastRun.add(lastRunTracker);
					}
					counter++;
				}
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			for(int i=0;i<lastRun.size();i++){
				System.out.println(lastRun.get(i).doubleValue());
			}
			
			
			System.out.println(100*mean(lastRun));
			System.out.println(sd(lastRun) + "%");
			System.out.println("�"+100*confidenceInterval(sd(lastRun)));
			
		
			
	    
	 // Confidence interval end
		
		
		int runCount=-1;
		for(int rc = 0;rc<runCount;rc++){
		//Setting environment as Clarke-Tax (0)
		Environment env = new Environment(0);
		
		//Get Environment Type
		System.out.println("The environment is a/an " + env.envTypeText(env.getEnvType()) + " environment");
		
		//Setting Agents
		ArrayList<ClarkeTaxAgent> agents = new ArrayList<>();
		for (int i = 0; i< 20; i++){
			ClarkeTaxAgent agent = new ClarkeTaxAgent(i);
			agents.add(agent);
		}
		
		//Get Types of Agents
		//for (int i = 0; i< agents.size(); i++){
		//	System.out.println("Agent " + i + ": " + agents.get(i).getAgentType());
		//}
		
		//Setting Content Co-owners
		ArrayList<Content> contents = new ArrayList<>();
		for(int a = 0;a<11000;a++){
		Random r = new Random();
		ArrayList<Integer> coowners = new ArrayList<>();
		
		//this part is for test, should be removed later on
		coowners.add(0);
		coowners.add(1);
		coowners.add(2);
		coowners.add(3);
		coowners.add(4);
		
		//this part commented for test, should be added later on
		/*
		int j = r.nextInt(3)+2;
		for(int i = 0; i < j; ){
			Random rn = new Random();
			int k = rn.nextInt(agents.size());
			if(!coowners.contains(k)){
				coowners.add(k);
				i++;
			}
		}
		*/
		
		Collections.sort(coowners);
		String cowners = "";
		for (int i = 0; i< coowners.size(); i++){
			cowners += coowners.get(i);
			if (i< coowners.size() -1 )
				cowners+="-";
		}
		Content cont = new Content(a, cowners, coowners);
		contents.add(cont);
		}
		
		//Listing content and coowners
		//for (int i = 0; i< contents.size(); i++){
		//	System.out.println("Content " + i + ": " + contents.get(i).getCoownersString());
		//}
		
		env.runClarkTax(agents, contents);
		
		
		}
		/*
		
		ArrayList<Double> avgTrckr = new ArrayList<Double>();
		double averageTracker=0;
			BufferedReader reader;
			try {
				reader = new BufferedReader(new FileReader(
						"D:\\1Success.txt"));
				String line = "-";
				int counter=0;
				while (line != null) {
					//System.out.println(ct +"-" + line);
					// read next line
					line = reader.readLine();
					if(counter < 10000){
						averageTracker=Double.valueOf(line);
						avgTrckr.add(averageTracker);
					}
					else if (counter < 500000)
					{	
						averageTracker = avgTrckr.get(counter % 10000) + Double.valueOf(line);
						avgTrckr.set((counter % 10000),averageTracker);
					}
					counter++;
				}
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		

			FileWriter fw = new FileWriter("D:\\ALT231Success.txt", true);
		    BufferedWriter bw = new BufferedWriter(fw);
		    PrintWriter out = new PrintWriter(bw);
		for(int k=0;k<avgTrckr.size();k++){
			avgTrckr.set((k),avgTrckr.get(k)/50);
			out.println(avgTrckr.get(k));
		}
	    
	    out.close();
	    
	    
	    
	    ArrayList<Double> avgTrckr2 = new ArrayList<Double>();
		double averageTracker2=0;
			BufferedReader reader2;
			try {
				reader2 = new BufferedReader(new FileReader(
						"D:\\1Satisfaction.txt"));
				String line = "-";
				int counter=0;
				while (line != null) {
					//System.out.println(ct +"-" + line);
					// read next line
					line = reader2.readLine();
					if(counter < 10000){
						averageTracker2=Double.valueOf(line);
						avgTrckr2.add(averageTracker2);
					}
					else if (counter < 500000)
					{	
						averageTracker2 = avgTrckr2.get(counter % 10000) + Double.valueOf(line);
						avgTrckr2.set((counter % 10000),averageTracker2);
					}
					counter++;
				}
				reader2.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		

			FileWriter fw2 = new FileWriter("D:\\ALT231Satisfaction.txt", true);
		    BufferedWriter bw2 = new BufferedWriter(fw2);
		    PrintWriter out2 = new PrintWriter(bw2);
		for(int k=0;k<avgTrckr2.size();k++){
			avgTrckr2.set((k),avgTrckr2.get(k)/50);
			out2.println(avgTrckr2.get(k));
		}
	    
	    out2.close();
		*/
	}
	
	public static double mean (ArrayList<Double> table)
    {
		double total = 0;

        for ( int i= 0;i < table.size(); i++)
        {
            double currentNum = table.get(i);
            total+= currentNum;
        }
        return (double)total/ (double)table.size();
    }
	
	public static double sd (ArrayList<Double> table)
	{
	    // Step 1: 
	    double mean = mean(table);
	    double temp = 0;

	    for (int i = 0; i < table.size(); i++)
	    {
	        double val = table.get(i);

	        // Step 2:
	        double squrDiffToMean = Math.pow(val - mean, 2);

	        // Step 3:
	        temp += squrDiffToMean;
	    }

	    // Step 4:
	    double meanOfDiffs = (double) temp / (double) (table.size());

	    // Step 5:
	    return Math.sqrt(meanOfDiffs);
	}
	
	public static double confidenceInterval (Double sd)
	{
		//Z for 95% confidence
	    double Z = 1.960;
	    //50 is the number of runs
	    return Z*sd / Math.sqrt(50);
	}

}
