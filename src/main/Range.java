package main;

public class Range {
		
		public int minRange=0;
		public int maxRange=0;
		private double utility=0;
		private double numberOfTries=0.0001;
		private double numberOfSuccess=0;
		private double distanceCoef = 0;
		private double mean = 0;
		
		
		public Range(int minRange,int maxRange,double utility){
			this.minRange = minRange;
			this.maxRange = maxRange;
			this.mean = ((double)minRange+ (double)maxRange)/(double)2;
			this.utility = utility;
		}
		
		public void setUtility(){
			this.utility = this.distanceCoef*this.numberOfSuccess / this.numberOfTries;
		}
		
		public double getUtility(){
			return this.utility*this.distanceCoef;
			//return this.utility;
		}
		
		public double getEfficiency(){
			return this.numberOfSuccess/this.numberOfTries;
			//return this.utility;
		}
		
		public double getRealUtility(){
			return this.utility;
		}
		
		public double getMean(){
			return this.mean;
		}
		
		public void updateUtility(double utility){
			this.utility = utility;
		}
		
		public void incNumberOfTries(){
			this.numberOfTries++;
		}
		
		public void incNumberOfSuccess(double coef){
			this.numberOfSuccess = this.numberOfSuccess + 1*coef;
		}
		
		public void setDistanceCoef(double disCo){
			this.distanceCoef = disCo;
		}
		
		public double getDistanceCoef(){
			return this.distanceCoef;
		}
		
		public void addTries(double tryVal){
			this.numberOfTries += tryVal;
		}
		
		public void addSuccess(double successVal){
			this.numberOfSuccess += successVal;
		}
		
		

}
