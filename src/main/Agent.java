package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import main.Environment.envTypeText;
import main.Range;


public class Agent {
	
	protected int id;
	//Unique id number of the agent
	
	public Agent(){
		
	}
	

	static class ClarkeTaxAgent extends Agent {
		
		enum agentTypeEnum {
			  MARGINALLYCONCERNED,
			  AMATEUR,
			  TECHNICIAN,
			  LAZYEXPERT,
			  FUNDAMENTALIST
			}

		public static class Bidscore{
			String owners = "";
			Integer bidScoreP = 0;
		}
		
		private List<Bidscore> bidSp;
		private int agentType;
		private int knowledge;
		private int motivation;
		private int decision;
		private ArrayList<Range> rangesShare;
		private ArrayList<Range> rangesNoShare;
		private double confidence;
		private int decisionsMadeShare = 0;
		private int decisionsMadeNoShare = 0;
		private int totalAsked = 0;
		private int totalAnswered = 0;
		private int totalCorrectAnswered = 0;
		private int totalCorrectBid = 0;
		private int totalIncorrectBid = 0;
		
		
		public ClarkeTaxAgent(int id){
			this.id = id;
			this.bidSp = new ArrayList<Bidscore>();
			this.agentType = setAgentType();
			
			//this part is for test, should be removed later on
			if(id == 0){
				this.agentType = 4;
				this.knowledge = 70;
				this.motivation = 70;
			}
			
			else{
				this.agentType = 4;
				this.knowledge = 70;
				this.motivation = 70;
			}
			
			this.rangesShare =  new ArrayList<Range>();
			this.rangesNoShare =  new ArrayList<Range>();
			initRanges();
			
			//decision in constructor, will change later on, now only one context so makes sense here
			Random r = new Random();
			int i = r.nextInt(2);
			this.decision = i;
			
			//this part is for test, should be removed later on
			if(id <2)
				this.decision = 0;
			else
				this.decision = 1;
			
		}
		
		public int setAgentType(){
			Random r = new Random();
			int j = r.nextInt(101);
			if(j<23){
				this.knowledge = 10;
				this.motivation = 10;
				return 0;
			}
			if(j<57){
				this.knowledge = 40;
				this.motivation = 40;
			return 1;
			}
			if(j<75){
				this.knowledge = 40;
				this.motivation = 70;
				return 2;
			}
			if(j<96){
				this.knowledge = 70;
				this.motivation = 10;
				return 3;
			}
			this.knowledge = 70;
			this.motivation = 70;
			return 4;
		}
		
		public String getAgentType(){
			return agentTypeEnum.values()[this.agentType].toString();
		}
		
		public void initRanges(){
			for(int i=0;i<20;i++){
				for(int j=i+1;j<=20;j++){
					Range tempRange = new Range(i,j,0);
					this.rangesShare.add(tempRange);
					this.rangesNoShare.add(tempRange);
				}
			}
		}
		
		public int getDecision(){
			this.totalAsked++;
			Random r = new Random();
			int i = r.nextInt(100);
			Random r2 = new Random();
			int j = r2.nextInt(100);
			System.out.println(i + "----" + j);
			System.out.println(this.motivation + "----" + this.knowledge);
			if(i<this.motivation){
				this.totalAnswered++;
				 if(j<this.knowledge){
					if(this.decision == 1)
						this.decisionsMadeShare++;
					if(this.decision == 0)
						this.decisionsMadeNoShare++;
					totalCorrectAnswered++;
					return this.decision;
					
				 }
				 else{
					 Random r3 = new Random();
					 int k = r3.nextInt(2);
					 //By chance correct in this case
					 if(k == this.decision)
						 totalCorrectAnswered++;
					 if(k == 1)
						 this.decisionsMadeShare++;
						if(k == 0)
							this.decisionsMadeNoShare++;
					 return k;
				 }
			}
			else
				return -1;
		}
		public int getActualDecision(){
				return this.decision;
		}
		
		public int rangeBid(String coownerString){
			if(totalAnswered == 0){
				System.out.println("Don't have knowledge to bid");
				return 0;
			}
			else if(decisionsMadeNoShare>= decisionsMadeShare){
				return rangeNoShareBid(coownerString);
			}
			else
				return rangeShareBid(coownerString);
				
		}
		
		public int rangeNoShareBid(String coownerString){
			System.out.println("Bidding for no share");
			if(this.decision == 0)
				this.totalCorrectBid++;
			else
				this.totalIncorrectBid++;
			System.out.println("No share before: " + this.decisionsMadeNoShare + " Share before: " + this.decisionsMadeShare
					+ " Total answered: " + this.totalAnswered);
			this.confidence = ((double)this.decisionsMadeNoShare/(double)(this.decisionsMadeNoShare+(double)this.decisionsMadeShare))*(1-Math.pow(Math.E, -((double)this.totalAnswered/10)));
			System.out.println("Confidence is: " + this.confidence);
			int closestRangeIndex = -1;
			double closestRangeValue = 999;
			for(int i = 0;i<rangesNoShare.size();i++){
				//System.out.println(rangesNoShare.get(i).minRange + "----" + rangesNoShare.get(i).maxRange + i + "----" + this.confidence+ "--- "+rangesNoShare.get(i).getEfficiency() + " --- " + rangesNoShare.get(i).getMean()/(double)20);
				//System.out.println(closestRangeIndex +"---"+closestRangeValue);
				
				//System.out.println(rangesNoShare.get(i).minRange + "----" + rangesNoShare.get(i).maxRange + ":" + this.confidence+ "--- "+rangesNoShare.get(i).getEfficiency() + " --- " + rangesNoShare.get(i).getMean()/(double)20);
				//System.out.println(Math.abs((this.confidence+rangesNoShare.get(i).getEfficiency())/2 - rangesNoShare.get(i).getMean()/(double)20));
				//if(closestRangeIndex != -1)
				//System.out.println(rangesNoShare.get(closestRangeIndex).minRange + "----" + rangesNoShare.get(closestRangeIndex).maxRange);

				if(rangesNoShare.get(i).getEfficiency()>0){
				if(Math.abs((this.confidence+rangesNoShare.get(i).getEfficiency())/2 - rangesNoShare.get(i).getMean()/(double)20) < closestRangeValue){
					closestRangeIndex = i;
					closestRangeValue = Math.abs((this.confidence+rangesNoShare.get(i).getEfficiency())/2 - rangesNoShare.get(i).getMean()/(double)20);
				}
				}
				else{
					if(Math.abs(this.confidence - rangesNoShare.get(i).getMean()/(double)20) < closestRangeValue){
						closestRangeIndex = i;
						closestRangeValue = Math.abs(this.confidence - rangesNoShare.get(i).getMean()/(double)20);
					}
				}
			}
			for(int i=0;i<this.bidSp.size();i++){
				if(coownerString.equals(this.bidSp.get(i).owners) ){
					System.out.println("teseting No Share:" + this.bidSp.get(i).bidScoreP + "---" +(int)rangesNoShare.get(closestRangeIndex).getMean());
					
					if(this.bidSp.get(i).bidScoreP < (int)rangesNoShare.get(closestRangeIndex).getMean()){
						System.out.println("Not enough budget, can't bid");
						return -this.bidSp.get(i).bidScoreP;
					}
					else{
						return -Math.min((int)rangesNoShare.get(closestRangeIndex).getMean(), this.bidSp.get(i).bidScoreP);
					}
				}
			}
				
			System.out.println("Picked range is: " + rangesNoShare.get(closestRangeIndex).minRange + "-" + rangesNoShare.get(closestRangeIndex).maxRange);
			return (int)-rangesNoShare.get(closestRangeIndex).getMean();
		}
		
		public int rangeShareBid(String coownerString){
			System.out.println("Bidding for share");
			if(this.decision == 1)
				this.totalCorrectBid++;
			else
				this.totalIncorrectBid++;
			System.out.println("No share before: " + this.decisionsMadeNoShare + " Share before: " + this.decisionsMadeShare
					+ " Total answered: " + this.totalAnswered);
			this.confidence = ((double)this.decisionsMadeShare/(double)(this.decisionsMadeNoShare+(double)this.decisionsMadeShare))*(1-Math.pow(Math.E, -((double)this.totalAnswered/10)));
			System.out.println("Confidence is: " + this.confidence);
			int closestRangeIndex = -1;
			double closestRangeValue = 999;
			for(int i = 0;i<rangesShare.size();i++){
				//System.out.println("hasdad: " + i + "----" + (this.confidence+rangesShare.get(i).getEfficiency())/2 + " --- " + rangesShare.get(i).getMean()/(double)20);
				//System.out.println(closestRangeIndex +"---"+closestRangeValue);
				
				if(rangesShare.get(i).getEfficiency()>0){
					if(Math.abs((this.confidence+rangesShare.get(i).getEfficiency())/2 - rangesShare.get(i).getMean()/(double)20) < closestRangeValue){
						closestRangeIndex = i;
						closestRangeValue = Math.abs((this.confidence+rangesShare.get(i).getEfficiency())/2 - rangesShare.get(i).getMean()/(double)20);
					}
					}
					else{
						if(Math.abs(this.confidence - rangesShare.get(i).getMean()/(double)20) < closestRangeValue){
							closestRangeIndex = i;
							closestRangeValue = Math.abs(this.confidence - rangesShare.get(i).getMean()/(double)20);
						}
					}
			}
			for(int i=0;i<this.bidSp.size();i++){
				if(coownerString.equals(this.bidSp.get(i).owners) ){
					System.out.println("teseting Share:" + this.bidSp.get(i).bidScoreP + "---" +(int)rangesNoShare.get(closestRangeIndex).getMean());
					
					if(this.bidSp.get(i).bidScoreP < (int)rangesShare.get(closestRangeIndex).getMean()){
						System.out.println("Not enough budget, can't bid");
						return this.bidSp.get(i).bidScoreP;
					}
					else{
						return Math.min((int)rangesShare.get(closestRangeIndex).getMean(), this.bidSp.get(i).bidScoreP);
					}
				}
			}
			System.out.println("Picked range is: " + rangesShare.get(closestRangeIndex).minRange + "-" + rangesShare.get(closestRangeIndex).maxRange);
			return (int)rangesShare.get(closestRangeIndex).getMean();
		}
		

		public int gettotalAsked(){
			return this.totalAsked;
		}
		
		public int gettotalAnswered(){
			return this.totalAnswered;
		}
		
		public int gettotalCorrectAnswered(){
			return this.totalCorrectAnswered;
		}
		
		public int gettotalBid(){
			return this.totalCorrectBid + this.totalIncorrectBid;
		}
		
		public int gettotalCorrectBid(){
			return this.totalCorrectBid;
		}
		public void updateorAddBidScore(String pairs){
			Boolean coownersExist = false;
			for(int i=0;i<this.bidSp.size();i++){
				if(pairs.equals(this.bidSp.get(i).owners) ){
					System.out.println("Same co-owners exist. Adding 10 budget");
					System.out.println("Current budget is: " + this.bidSp.get(i).bidScoreP);
					this.bidSp.get(i).bidScoreP += 10;
					System.out.println("Updated budget is: " + this.bidSp.get(i).bidScoreP);
					i = this.bidSp.size();
					coownersExist = true;
				}
			}
			if(!coownersExist){
				System.out.println("New co-owners. Adding 10 budget");
				Bidscore tempBS = new Bidscore();
				tempBS.bidScoreP = 10;
				tempBS.owners = pairs;
				this.bidSp.add(tempBS);
			}
			//return 0;
		}
		
		public void modifyBidScore(String coownersString, int amount){
			for(int i=0;i<this.bidSp.size();i++){
				if(coownersString.equals(this.bidSp.get(i).owners) ){
					System.out.println("Current budget is: " + this.bidSp.get(i).bidScoreP);
					this.bidSp.get(i).bidScoreP -= amount;
					System.out.println("Updated budget is: " + this.bidSp.get(i).bidScoreP);
					i = this.bidSp.size();
				}
			}
		}
		
		
		public Integer getPairwiseBidScore(String pairs){
			
			for(int i=0;i<this.bidSp.size();i++){
				if(pairs.equals(this.bidSp.get(i).owners) ){
					return this.bidSp.get(i).bidScoreP;
				}
			}
			return 0;
		}
		
		
		public void updateEfficienyNoShRange(double coef){
			int closestRangeIndex = -1;
			double closestRangeValue = 999;
			for(int i = 0;i<rangesNoShare.size();i++){
				if(rangesNoShare.get(i).getEfficiency()>0){
				if(Math.abs((this.confidence+rangesNoShare.get(i).getEfficiency())/2 - rangesNoShare.get(i).getMean()/(double)20) < closestRangeValue){
					closestRangeIndex = i;
					closestRangeValue = Math.abs((this.confidence+rangesNoShare.get(i).getEfficiency())/2 - rangesNoShare.get(i).getMean()/(double)20);
				}
				}
				else{
					if(Math.abs(this.confidence - rangesNoShare.get(i).getMean()/(double)20) < closestRangeValue){
						closestRangeIndex = i;
						closestRangeValue = Math.abs(this.confidence - rangesNoShare.get(i).getMean()/(double)20);
					}
				}
			}
			rangesNoShare.get(closestRangeIndex).addTries(1);
			rangesNoShare.get(closestRangeIndex).addSuccess(coef);
		}
		
		public void updateEfficienyShRange(double coef){
			int closestRangeIndex = -1;
			double closestRangeValue = 999;
			for(int i = 0;i<rangesShare.size();i++){
				if(rangesShare.get(i).getEfficiency()>0){
					if(Math.abs((this.confidence+rangesShare.get(i).getEfficiency())/2 - rangesShare.get(i).getMean()/(double)20) < closestRangeValue){
						closestRangeIndex = i;
						closestRangeValue = Math.abs((this.confidence+rangesShare.get(i).getEfficiency())/2 - rangesShare.get(i).getMean()/(double)20);
					}
					}
					else{
						if(Math.abs(this.confidence - rangesShare.get(i).getMean()/(double)20) < closestRangeValue){
							closestRangeIndex = i;
							closestRangeValue = Math.abs(this.confidence - rangesShare.get(i).getMean()/(double)20);
						}
					}
			}
			rangesShare.get(closestRangeIndex).addTries(1);
			rangesShare.get(closestRangeIndex).addSuccess(coef);
		}
	}

}
