package main;

import java.util.ArrayList;

public class Content {
	
	private int id;
	String coowners;
	ArrayList<Integer> cowners = new ArrayList<>();
	
	
	public Content(int id, String coowners, ArrayList<Integer> cowners){
		this.id = id;
		this.coowners = coowners;
		this.cowners = cowners;
	}
	
	public int getID(){
		return this.id;
	}
	
	public String getCoownersString(){
		return this.coowners;
	}
	
	public String getCoowners(){
		return this.coowners;
	}
	

}
